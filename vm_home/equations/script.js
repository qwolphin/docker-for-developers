var algebra = require('algebra.js');

var x1 = algebra.parse(process.argv[2]);
var x2 = algebra.parse(process.argv[3]);

var eq = new algebra.Equation(x1, x2);
console.log("Equation: " + eq.toString());

var answer = eq.solveFor("x");

console.log("Solutions: x = " + answer.toString());

import aioredis
from sanic import Sanic
from sanic.response import json
from broadcast import Broadcast

cheese_updates = Broadcast()
app = Sanic("cheese-shop")


@app.post("/ask_for/<name:str>")
async def handle_question(request, name):
    is_new = await redis.sadd("cheese", name)

    if not is_new:
        return json(f"You've already asked for {name}")

    await redis.publish("cheese_updates", f"Someone asked for {name}")
    return json(f"No, we don't have {name}")


@app.websocket("/cheese_updates")
async def listen_to_updates(request, ws):
    async for msg in cheese_updates.iter():
        await ws.send(msg)


async def handle_redis():
    global redis
    redis = await aioredis.from_url("redis://redis:6379", decode_responses=True)

    pubsub = redis.pubsub()
    await pubsub.subscribe("cheese_updates")

    async for msg in pubsub.listen():
        if msg["type"] == "message":
            cheese_updates.publish(msg["data"])

app.add_task(handle_redis())

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9000, debug=True)

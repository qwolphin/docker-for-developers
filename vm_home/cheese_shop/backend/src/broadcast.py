import asyncio


class Broadcast:
    def __init__(self):
        self.iterators = {}

    def publish(self, value):
        for i in self.iterators.values():
            i.put_nowait(value)

    async def iter(self):
        q = asyncio.Queue()
        q_id = id(q)
        self.iterators[id(q)] = q

        try:
            while True:
                value = await q.get()
                yield value
        finally:
            del self.iterators[q_id]

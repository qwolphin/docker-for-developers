set -e

curl -fsSL https://get.docker.com | sh

mkdir -p /usr/local/lib/docker/cli-plugins/
curl -SL https://github.com/docker/compose/releases/download/v2.2.2/docker-compose-linux-x86_64 -o /usr/local/lib/docker/cli-plugins/docker-compose
chmod +x /usr/local/lib/docker/cli-plugins/docker-compose

curl -SL https://github.com/vi/websocat/releases/download/v1.9.0/websocat_linux64 -o /usr/local/bin/websocat
chmod +x /usr/local/bin/websocat 

useradd --groups docker,sudo --shell /bin/bash -m user
echo -e "$1\n$1" | passwd user
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
sed -i 's/#Port 22/Port 1261/' /etc/ssh/sshd_config

mv /root/vm_home/* /home/user
chown -R user /home/user
docker pull node:17

reboot

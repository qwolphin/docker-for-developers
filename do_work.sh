set -e

for IP in $(cat ips.txt);
do
    echo "Working on $IP";
    scp -oStrictHostKeyChecking=no -r init_vm.sh vm_home/ root@$IP:/root;
    ssh -oStrictHostKeyChecking=no root@$IP bash /root/init_vm.sh very_secret_here;
done;

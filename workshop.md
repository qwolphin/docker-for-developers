---
# What is the fuss about

Prior art: FreeBSD jails

Containerisation - lightweight, low overhead, fast

In most cases based on Linux primitives - chroot(2), CGroups and Namespaces

Bundles the whole system image with an app

## Sshh, dear, don't cause a fuss. I'll have your spam. I love it.



---
## Containers vs traditional VM's

- uses the same kernel
- fast filesystem mounts between containers and host
- zero-overhead networking possible, overhead is lower
- usually FOSS (free-as-in-beer unlike VMWare)



---
# Practice

## Ruby REPL

Let's say for some reason we reallly want to know what the following Ruby program will output:

```
i = 1; i = ++i + ++i; puts "i=#{i}"
```

```
docker run -it ruby irb
```



---
# Basics: image and containers

What happened there?

Tagging, Image, Container, Registry

```
docker image ls
docker container ls
```



---
# Practice

## Let's make a custom image

Making an image that outputs our IP

```
cd ~/custom_image
docker build -t my_custom_image .
docker run my_custom_image
```



---
# UnionFS, layers

How image filesystem is stored

```
docker history my_custom_image
docker image inspect my_custom_image
```

## It's turtles all the way down



---
# Practice

Soloving cubic equations, but in JS

```
cd ~/equations
```

## Always Look On The Bright Side Of Life



---
## How to make subsequent build faster

Layers and cache invalidation

```
time docker build -f naive.Dockerfile .
time docker build -t solover .

docker run solover npm run main "6*x^2 + 17x + 12" "0"
```

```
docker build -t solover . && docker run solover npm run main "6*x^2 + 17x + 12" "0" 
```



---
## How to use Docker for local development

Code volumes and permissions

Make it work for `"spanishinquisition^2 + 12" "21"`

```
docker run solover npm run dev "spanishinquisition^2 + 12" "21"
docker run -it -v $(pwd):/app --workdir /app node:17 bash

--user=node
```

## Nobody expects the spanish inquisition



---
## Networking and SDN

Container networking context

How to make multiple containers work together

Endpoints, load-balancer IP

(instead of multiple A records)



---
## Hardcoding URL's is great!

DNS does the heavy lifting

```
requests.post('http://my-microservice/some_method')
```



---
# Practice

## Networking

Listen to requests:

```
docker run -it -d -p 8080:8080 thoba/todo-list-app
docker run -it --net=host thoba/todo-list-app
```

Access the HTTP:

```
wget -qO- https://api.ipify.org
docker run -it alpine wget -qO- http://localhost:8080
docker run -it --net=host alpine wget -qO- http://localhost:8080
```



---
## docker-compose: the icing on the cake 

What is container orchestration

`docker-compose` and `docker compose`



---
## Compose file

- `services`
- `networks`
- `volumes`



---
# Practice
## docker-compose

```
cd ~/cheese_shop
docker-compose up
websocat ws://localhost:9000/cheese_updates
curl -X POST http://localhost:9000/ask_for/tilsit
curl -X POST http://localhost:9000/ask_for/brie
curl -X POST http://localhost:9000/ask_for/cheddar
```

## I was deliberately wasting your time, sir



---
# Advanced topics

(just a quick mention)

- buildkit and distributed builds
- DinD and DooD
- macOS performance
- extension fields and inheretance 



---
# Kubernetes

Distributed container orchestration and a whole lot of different features

## I'm afraid I'm not personally qualified to confuse cats, but I can recommend an extremely good service
